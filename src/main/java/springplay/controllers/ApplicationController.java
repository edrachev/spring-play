package springplay.controllers;

import java.util.concurrent.CompletionStage;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import springplay.beans.LastPadded;
import springplay.forms.PadderForm;
import springplay.services.PadderHistoryService;
import springplay.services.PadderService;

@Controller
public class ApplicationController {
	private final PadderService padderService;
	private final PadderHistoryService padderHistoryService;
	private final LastPadded lastPadded;
	
	@Autowired
    public ApplicationController(PadderService padderService, PadderHistoryService padderHistoryService, LastPadded lastPadded) {
		this.padderService = padderService;
		this.padderHistoryService = padderHistoryService;
		this.lastPadded = lastPadded;
	}

	@RequestMapping("/")
    public String home(Model model) {
		model.addAttribute("padderForm", new PadderForm(lastPadded.getInput(), ""));
    	return "main";
    }

	@RequestMapping("/history")
    public String history(Model model) {
		model.addAttribute("historyItems", padderHistoryService.findAll());
    	return "history";
    }
	
    @RequestMapping("/padding")
    public String padding(Model model, @Valid PadderForm form, BindingResult bindingResult) {
    	String output = padderService.padString(form.getInput());
    	lastPadded.setInput(form.getInput());
    	form.setOutput(output);
    	model.addAttribute("padderForm", form);
    	return "main";
    }
    
    /*
    @RequestMapping("/padding")
    public CompletionStage<String> padding(Model model, @Valid PadderForm form, BindingResult bindingResult) {
    	lastPadded.setInput(form.getInput());
    	return padderService.padStringAsync(form.getInput())
    		.thenApply(output -> {
    			model.addAttribute("padderForm", form.withOutput(output));
    			return "main";
    		});
    }
    */
}
