package springplay.beans;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LastPadded {
	private String input;
	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}
}
