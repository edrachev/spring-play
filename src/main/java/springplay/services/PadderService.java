package springplay.services;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import springplay.websocket.SocketHandler;

@Service
public class PadderService {
	private final PadderHistoryService historyService;
	private SocketHandler socketHandler;
	
	@Value("${prop.one}")
	private String defaultValue;
	
	@Autowired
	public PadderService(PadderHistoryService historyService, SocketHandler socketHandler) {
		super();
		this.historyService = historyService;
		this.socketHandler = socketHandler;
	}

	public String padString(String str) {
		String result = StringUtils.leftPad(str, 10, "*");
		historyService.log(str, result);
		return result;
	}

	public CompletionStage<String> padStringAsync(String str) {
		return CompletableFuture.supplyAsync(() -> {
			String paddedString = StringUtils.leftPad(str, 10, "*");
			socketHandler.sendMessage(str, paddedString);
			historyService.log(str, paddedString);
			return paddedString;
		});
	}
}
