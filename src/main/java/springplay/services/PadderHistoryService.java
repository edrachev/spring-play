package springplay.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springplay.persistence.PadderHistory;
import springplay.persistence.PadderHistoryRepository;

@Service
public class PadderHistoryService {
	
	private final PadderHistoryRepository repository;

	@Autowired
	public PadderHistoryService(PadderHistoryRepository repository) {
		super();
		this.repository = repository;
	}

	public void log(String input, String output) {
		PadderHistory item = new PadderHistory(input, output);
		repository.save(item);
	}
	
	public List<PadderHistory> findAll() {
		return repository.findAll();
	}
}