package springplay.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TracingAdvice {
	
	@Around("execution(* springplay.services.PadderService.*(..))")
	public Object trace(ProceedingJoinPoint pjp) throws Throwable {
		System.out.println(pjp.getSignature().getName());
		Object retVal = pjp.proceed();
        return retVal;
	}
}
